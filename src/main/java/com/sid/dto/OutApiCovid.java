package com.sid.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OutApiCovid implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("Cases")
	private int cases;
	
	@JsonProperty("Date")
	private String date;
	
	@JsonProperty("Status")
	private String status;

	public OutApiCovid() {
		super();
	}

	public OutApiCovid(int cases, String date, String status) {
		super();
		this.cases = cases;
		this.date = date;
		this.status = status;
	}

	public int getCases() {
		return cases;
	}

	public void setCases(int cases) {
		this.cases = cases;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OutApiCovid [cases=" + cases + ", date=" + date + ", status=" + status + "]";
	}

}
