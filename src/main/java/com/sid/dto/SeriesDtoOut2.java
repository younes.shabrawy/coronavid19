package com.sid.dto;

import java.io.Serializable;
import java.util.List;

public class SeriesDtoOut2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<String> date;
	private List<Integer> cases;

	public SeriesDtoOut2() {
		super();
	}

	public SeriesDtoOut2(List<String> date, List<Integer> cases) {
		super();
		this.date = date;
		this.cases = cases;
	}

	public List<String> getDate() {
		return date;
	}

	public void setDate(List<String> date) {
		this.date = date;
	}

	public List<Integer> getCases() {
		return cases;
	}

	public void setCases(List<Integer> cases) {
		this.cases = cases;
	}

	@Override
	public String toString() {
		return "SeriesDtoOut2 [date=" + date + ", cases=" + cases + "]";
	}

	

}
