package com.sid.dto;

import java.io.Serializable;

public class SeriesDtoOut implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String date;
	private int cases;

	public SeriesDtoOut() {
		super();
	}

	public SeriesDtoOut(String date, int cases) {
		super();
		this.date = date;
		this.cases = cases;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCases() {
		return cases;
	}

	public void setCases(int cases) {
		this.cases = cases;
	}

	@Override
	public String toString() {
		return "SeriesDtoOut [date=" + date + ", cases=" + cases + "]";
	}

}
