package com.sid.ressources;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sid.dto.SeriesDtoOut2;
import com.sid.service.CallApiService;

@RestController
@CrossOrigin("*")
public class CovidController {
	@Autowired
	CallApiService callApiService;
	
	@GetMapping(value="/covid19")
	public SeriesDtoOut2 getData() throws URISyntaxException{
		return callApiService.callApi2();
	}
}
