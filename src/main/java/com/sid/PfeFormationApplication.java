package com.sid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PfeFormationApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(PfeFormationApplication.class, args);
	}

}
