package com.sid.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sid.dto.OutApiCovid;
import com.sid.dto.SeriesDtoOut2;
import com.sid.helper.Mapper;


@Service
public class CallApiService {
	@Autowired
	RestTemplate restTemplate;

	@Value("${covid.api.url}")
	public String url;
	@Autowired
	Mapper mapper;
	public SeriesDtoOut2 callApi2() throws URISyntaxException {

		List<OutApiCovid> retour;
		URI uri = new URI(url);
		ResponseEntity<OutApiCovid[]> response = restTemplate.getForEntity(uri, OutApiCovid[].class);
		retour = Arrays.asList(response.getBody());
		return mapper.mapOutApiCovidToSeriesDtoOut2(retour);

	}
}
