package com.sid.helper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sid.dto.OutApiCovid;
import com.sid.dto.SeriesDtoOut;
import com.sid.dto.SeriesDtoOut2;

@Service
public class Mapper {

		public SeriesDtoOut mapOutApiCovidToSeriesDtoOut(OutApiCovid outApiCovid) {
			
			
			SeriesDtoOut reponse  = new SeriesDtoOut();
			
			reponse.setCases(outApiCovid.getCases());
			LocalDate day = LocalDateTime.parse(outApiCovid.getDate().replace("Z", "")).toLocalDate();
			reponse.setDate(day.toString());
			return reponse;
					
		}
		
		
		public SeriesDtoOut2 mapOutApiCovidToSeriesDtoOut2(List<OutApiCovid> outApiCovids) {
			
			SeriesDtoOut2 reponse2  = new SeriesDtoOut2();
			List<Integer> casess = new ArrayList<>();
			List<String> dat =new ArrayList<>();
			outApiCovids.forEach(action -> {
				casess.add(mapOutApiCovidToSeriesDtoOut(action).getCases());
				dat.add(mapOutApiCovidToSeriesDtoOut(action).getDate());
			});
			
			reponse2.setCases(casess);
			reponse2.setDate(dat);
			return reponse2;
					
		}
		
		
		
		public List<SeriesDtoOut> mapOutApiCovidToSeriesDtoOut(List<OutApiCovid> list){
			List<SeriesDtoOut> response = new ArrayList<>();
			list.forEach( outApiCovid -> response.add(mapOutApiCovidToSeriesDtoOut(outApiCovid)));
			return response;
		}
}
